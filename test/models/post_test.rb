require 'test_helper'

class PostTest < ActiveSupport::TestCase
    test "Valid" do
    	post = Post.new(title: 'test', body: 'test')
  	  	assert post.valid?
    end
  
	test 'no title' do
	    post = Post.new(title: '', body: 'test')
	  	assert_not post.valid?
	end  	

	test 'no body' do
	    post = Post.new(title: 'test', body: '')
	  	assert_not post.valid?
	end  	
end
