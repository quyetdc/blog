require 'test_helper'

class CommentTest < ActiveSupport::TestCase
    test "Valid" do
    	post = Post.create(title: 'test', body: 'test')
    	comment = Comment.new(body: 'test', post: post)
  	  	assert comment.valid?
    end
  
	test 'no post' do    	
    	comment = Comment.new(body: 'test', post_id: 10000)
  	  	assert_not comment.valid?
  	end  	

	test 'no body' do
	    post = Post.create(title: 'test', body: 'test')
    	comment = Comment.new(body: '', post: post)
  	  	assert_not comment.valid?
	end  	
end
